/* This program uses a genaric Cars class to generate a List using Arraylist, a Set using TreeList,
and a queue using LinkedList. Then, a map of service technicians and managers are is created using TreeMap.
The program displays all the cars in inventory, the individual car types, the cars in the service queue,
and a list of service technicians and their managers. The user is prompted if they want to proceed with displaying
the lists.
 */

package byui.cmikolyski;
import java.util.*;

public class JavaCollections {

    //Create Cars class to use for generics in the Lists.
    public static class Cars {

        private String make;
        private String model;

        public Cars(String make, String model) {

            this.make = make;
            this.model = model;
        }

        public String toString() {
            return make + " " + model;

        }


    }
    // Create comparitor for Cars
    public static class Compare implements Comparator<Cars>{

    @Override
    public int compare(Cars e1, Cars e2) {
      return e1.make.toUpperCase().compareTo(e2.make.toUpperCase());

    }
    }
    // Main method for JavaCollections class.
    static void myCollections()throws Exception {

    //Make the list of cars using generic car class.
    List<Cars> arrl = new ArrayList<>();

    // Add cars to the list of cars
        arrl.add(new Cars("subaru", "Outback"));
        arrl.add(new Cars("Toyota", "Land Cruiser"));
        arrl.add(new Cars("Ford", "Mustang"));
        arrl.add(new Cars("Dodge", "Ram 2500"));
        arrl.add(new Cars("Ford", "Mustang"));
        arrl.add(new Cars("Buick", "Enclave"));
        arrl.add(new Cars("Subaru", "Outback"));


    //Make set from list and using comparitor.
    Set<Cars> arrs = new TreeSet<>(new Compare()) {
    };
        for (int i = 0; i < arrl.size(); i++) {
        arrs.add(arrl.get(i));
    }


    //Make the service queue from list
    Queue<String> arrq = new LinkedList<>();
    //populate the queue.
        for (int i = 0; i < arrl.size(); i++) {
        arrq.add(arrl.get(i).toString());
    }


    //Make the first list of service techs.
    Set<String> serviceTech = new TreeSet<>();
        serviceTech.add("Mike M.");
        serviceTech.add("Leo B.");
        serviceTech.add("Donny T.");
        serviceTech.add("Raf T.");

    //Make the second list of service techs.
    Set<String> serviceTech2 = new TreeSet<>();
        serviceTech2.add("Aaron A.");
        serviceTech2.add("Bobby J.");
        serviceTech2.add("Charles X");
        serviceTech2.add("Aaron B.");

    //Make the map
    Map<String, Set> mapServiceM = new TreeMap<>();
        mapServiceM.put("Jason", serviceTech);
        mapServiceM.put("Alex", serviceTech2);

        //Print the list of cars in inventory
        System.out.println("List of all cars in inventory");
        for (Cars list : arrl) {
            System.out.println(list);
        }
        System.out.println();

        //print the set of car types from inventory
        System.out.println("List of car types from total inventory.");
        for (Cars set : arrs) {
            System.out.println(set);
        }
        System.out.println();

        //print the service queue
        System.out.println("Next car in line to be serviced");
        System.out.println(arrq.peek());
        System.out.printf("%nService on " + arrq.poll() + " Complete.%n");
        System.out.println("Adding vehicle to service queue");

        //Adding car to demonstrate Queue Fifo ability.
        arrq.add(new Cars("Ford", "F150").toString());
        System.out.println("Remaining cars in Service queue:");
        for (String queue : arrq) {
            System.out.println(queue);
        }
        System.out.println("Next car in line to be serviced:");
        System.out.println(arrq.peek());

        System.out.println("List of Service Technicians and Manager responsible for Technicians.");
        for (Map.Entry<String, Set> map : mapServiceM.entrySet()){
            System.out.println("Manager: " + map.getKey() + ": "
                    + "Technician " +map.getValue().toString()
                    .replace("[", "").replace("]", ""));
        }

}



    public static void main(String [] args)throws Exception {
        Boolean run;
        Scanner myScanner = new Scanner(System.in);



        do {
            run = true;


                System.out.println("Would you like to see a list of cars in inventory and in maintenance queue?(Yes, or No.)");
                String userInput = myScanner.nextLine();
                if (userInput.equalsIgnoreCase("Yes") ) {
                    try {
                        myCollections();
                    } catch (Exception e) {
                        System.out.println("There is something wrong with the collections");
                        System.exit(0);
                    }
                }
                else if (userInput.equalsIgnoreCase("No")) {
                    System.out.println("Good Bye!");
                    System.exit(0);

                }
                else {
                    System.out.println("Please enter the Yes or No.");
                }

        } while (run);
    }
}
